#!/usr/bin/env node

const os = require('node:os')
const fs = require('node:fs')
const { execSync } = require('node:child_process')

if (cmdExists('code')) {
  fs.rmSync(`${process.cwd()}/.vscode`, { recursive: true, force: true }, err => { })
  fs.mkdirSync(`${process.cwd()}/.vscode`)
  fs.writeFileSync(`${process.cwd()}/.vscode/extensions.json`,
    `
{
  "recommendations": ["Vue.volar", "Vue.vscode-typescript-vue-plugin"]
}
`, () => {
    console.log(`✔ 成功配置本地 vscode`)
  })
}
console.log('✔ 完成 VSCode 配置`')

const extensions = [
  { id: 'Vue.volar', desc: 'Volar' },
  { id: 'dbaeumer.vscode-eslint', desc: 'ESLint' },
  { id: 'EditorConfig.EditorConfig', desc: 'EditorConfig' },
  { id: 'bradlc.vscode-tailwindcss', desc: 'Tailwind CSS IntelliSense' }
]
const installed = Buffer.from(execSync('code --list-extensions'), 'utf-8').toString().split(/[\s\n]/)

for (const extension of extensions) {
  if (installed.indexOf(extension.id) === -1) {
    execSync(`code --install-extension ${extension.id}`)
    console.log(`✔ 成功安装 ${extension.desc}`)
  } else {
    console.log(`✔ ${extension.desc} 已安装`)
  }
}
console.log('✔ 完成插件安装`')

function cmdExists(cmd) {
  try {
    execSync(
      os.platform() === 'win32'
        ? `cmd /c "(help ${cmd} > nul || exit 0) && where ${cmd} > nul 2> nul"`
        : `command -v ${cmd}`,
    )
    return true
  }
  catch {
    return false
  }
}
