import path from 'node:path'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueDevTools from 'vite-plugin-vue-devtools'
import tailwindcss from '@tailwindcss/vite'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vite.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueDevTools(),
    tailwindcss(),
    AutoImport({
      imports: [
        'vue',
        'vue-router',
        'pinia',
        '@vueuse/core'
      ],
      dirs: [
        'src/composables',
        'src/stores'
      ],
      resolvers: [ElementPlusResolver()],
      dts: path.resolve(__dirname, 'types/auto-imports.d.ts')
    }),
    Components({
      dirs: [
        'src/components'
      ],
      resolvers: [ElementPlusResolver()],
      dts: path.resolve(__dirname, 'types/components.d.ts')
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    },
  },
})
