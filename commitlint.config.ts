import type { UserConfig } from '@commitlint/types'

const Configuration: UserConfig = {
  extends: ['@commitlint/config-conventional'],
  rules: {},
  prompt: {
    settings: {},
    messages: {
      skip: ':skip',
      max: 'upper %d chars',
      min: '%d chars at least',
      emptyWarning: 'can not be empty',
      upperLimitWarning: 'over limit',
      lowerLimitWarning: 'below limit'
    },
    questions: {
      type: {
        description: "Select the type of change that you're committing:",
        enum: {
          feat: {
            description: '新增功能',
            title: 'Features',
            emoji: '✨',
          },
          fix: {
            description: '修复 bug',
            title: 'Bug Fixes',
            emoji: '🐛',
          },
          docs: {
            description: '文档修改',
            title: 'Documentation',
            emoji: '📚',
          },
          style: {
            description: '代码格式修改(如空格、分号等，不是 css)',
            title: 'Styles',
            emoji: '💎',
          },
          refactor: {
            description: '代码更改(既没有修复错误也没有添加功能)',
            title: 'Code Refactoring',
            emoji: '📦',
          },
          perf: {
            description: '提高性能的代码更改',
            title: 'Performance Improvements',
            emoji: '🚀',
          },
          build: {
            description: '影响构建系统或外部依赖项的变化(示例范围: vite、npm)',
            title: 'Builds',
            emoji: '🛠',
          },
          ci: {
            description: 'CI/CD 配置文件和脚本的更改(示例范围: Travis、GitHub Actions)',
            title: 'Continuous Integrations',
            emoji: '⚙️',
          },
          chore: {
            description: "不修改 src 或测试文件的其他更改",
            title: 'Chores',
            emoji: '♻️',
          },
          revert: {
            description: '恢复之前的提交',
            title: 'Reverts',
            emoji: '🗑',
          },
        },
      },
      scope: {
        description:
          'What is the scope of this change (e.g. component or file name)',
      },
      subject: {
        description: 'Write a short, imperative tense description of the change',
      },
      body: {
        description: 'Provide a longer description of the change',
      },
      isBreaking: {
        description: 'Are there any breaking changes?',
      },
      breakingBody: {
        description:
          'A BREAKING CHANGE commit requires a body. Please enter a longer description of the commit itself',
      },
      breaking: {
        description: 'Describe the breaking changes',
      },
      isIssueAffected: {
        description: 'Does this change affect any open issues?',
      },
      issuesBody: {
        description:
          'If issues are closed, the commit requires a body. Please enter a longer description of the commit itself',
      },
      issues: {
        description: 'Add issue references (e.g. "fix #123", "re #123".)',
      },
    }
  }
};

export default Configuration;
