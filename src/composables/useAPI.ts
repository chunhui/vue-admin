import { createFetch } from "@vueuse/core";

export const useAPI = createFetch({
  options: {
    immediate: false,
    async beforeFetch({options}) {
      options.headers = {
        'Content-Type': 'application/json'
      }
      return { options }
    },
    afterFetch(ctx) {
      console.log('afterFetch:',ctx)
      return ctx
    },
    onFetchError(ctx) {
      return ctx
    },
  }
})
