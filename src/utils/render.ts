import BaseIcon from "@/components/base/BaseIcon.vue";

/**
 * 根据传入的图标渲染
 * @example
 *  import IconAdd from '@/components/icons/IconAdd.vue'
 *
 *  renderIcon(IconAdd)
 * @param icon
 * @returns
 */
export function renderIcon(icon: Component) {
  return () => h(BaseIcon, null, { default: () => h(icon) });
}

/**
 * 根据传入的图片名进行渲染
 * @example
 *  // components/icons 文件夹里的才行
 *  renderIconWithName('IconAdd')
 * @param name
 * @param props
 * @returns
 */
export function renderIconWithName(name: string, props?: never) {
  const asyncIcon = defineAsyncComponent(() => import(`@/components/icons/Icon${name.replace('Icon', '')}.vue`))
  return () => h(BaseIcon, props, { default: () => h(asyncIcon) })
}
