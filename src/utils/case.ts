/**
 * 转换成小驼峰
 *
 * @see http://stackoverflow.com/questions/2970525/converting-any-string-into-camel-case
 */
export function camelCase(str: string, firstCapital: boolean = false): string {
  if (firstCapital) str = " " + str
  return str.replace(/^([A-Z])|[\s-_](\w)/g, function (match, p1, p2) {
    if (p2) return p2.toUpperCase()
    return p1.toLowerCase()
  })
}

/**
 * 蛇形命名
 */
export function snakeCase(str: string): string {
  return (
    str
      .replace(/([A-Z])([A-Z])([a-z])/g, "$1_$2$3")
      .replace(/([a-z0-9])([A-Z])/g, "$1_$2")
      .toLowerCase()
  )
}

/**
 * 标题大小写
 *
 * @see http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
 */
export function titleCase(str: string): string {
  return str.replace(
    /\w\S*/g,
    (txt) => txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase(),
  )
}
