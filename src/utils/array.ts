export function arrayToTree(data: never[], pid?: number | null) {
  return data
    .filter(item => item.pid === pid)
    .map(item => ({
      ...item,
      children: arrayToTree(item, item.id)
    }))
}
