import './styles/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import { setupRouter } from './router'
import { setupStore } from './stores'

async function bootstrap() {
  const app = createApp(App)

  // 挂载状态
  setupStore(app)

  // 挂载路由
  setupRouter(app)

  // 模拟数据
  const { worker } = await import('@/mocks/browser')
  await worker.start()

  app.mount('#app')
}

void bootstrap()
