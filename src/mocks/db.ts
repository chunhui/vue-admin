import { factory, nullable, oneOf, primaryKey } from '@mswjs/data'
import { faker } from "@faker-js/faker";

export const db = factory({
  user: {
    id: primaryKey(Number),
    username: faker.internet.username,
    password: String,
    createAt: faker.date.anytime,
    profile: oneOf('profile')
  },
  profile: {
    id: primaryKey(Number),
    email: faker.internet.email,
    phone: faker.phone.number,
    gender: faker.datatype.boolean,
    birthday: faker.date.birthdate,
    avatar: faker.image.avatar,
    bio: nullable(faker.person.bio)
  },
  menu: {
    id: primaryKey(Number),
    pid: nullable<number>(() => null),
    title: String,
    name: String,
    icon: nullable<string>(() => null),
  }
})

db.user.create({ id: 1, username: 'admin', password: 'admin' })

// 菜单
db.menu.create({ id: 1, title: '仪表盘', name: 'dashboard', icon: 'IconDashboardFilled' })
db.menu.create({ id: 2, title: '系统管理', name: 'system' })
db.menu.create({ id: 3, pid: 2, title: '用户管理', name: 'user', icon: 'IconUserFilled' })
db.menu.create({ id: 4, pid: 2, title: '角色管理', name: 'role', icon: 'IconShieldFilled' })
db.menu.create({ id: 5, pid: 2, title: '菜单管理', name: 'menu' })
db.menu.create({ id: 6, pid: 2, title: '数据字典', name: 'dictionaries', icon: 'IconBookFilled' })
// db.menu.create({ id: 7, title: '', name: '' })
// db.menu.create({ id: 8, title: '', name: '' })
// db.menu.create({ id: 9, title: '', name: '' })
