import { delay, http } from "msw";
import { db } from "../db";
import { userHandler } from "./user.handler";

export const handlers = [
  http.all('*', async () => {
    await delay(300)
  }),

  ...userHandler,
  ...db.user.toHandlers('rest', '/api')
]
