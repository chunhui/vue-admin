import { http, HttpResponse } from "msw";
import { db } from "../db";

export const userHandler = [
  // 获取当前登录用户的信息
  http.get('/api/user/info', async () => {

  }),
  // 获取当前登录用户可访问的菜单
  http.get('/api/user/menus', async () => {
    return HttpResponse.json(db.menu.getAll())
  })
]

// function arrayToTree(items, parentId?: number) {
//   return items
//     .filter(item => item.pid === parentId)
//     .map(item => ({
//       ...item,
//       children: arrayToTree(items, item.id)
//     }));
// }
