import type { Language } from "element-plus/es/locales.mjs"

interface State {
  /**
   * 配置语言包
   */
  locale?: Language

  /**
   * 大小
   */
  size?: 'small' | 'default' | 'large'
}
export const useAppStore = defineStore('app',{
  state: (): State => {
    return {

    }
  }
})
